﻿using System.Threading.Tasks;

namespace CK.Sprite.Interceptor
{
	public abstract class AbpInterceptor : IAbpInterceptor
    {
        public abstract Task InterceptAsync(IAbpMethodInvocation invocation);
    }
}