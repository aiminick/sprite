using System;

namespace CK.Sprite.Interceptor
{
    public interface IOnServiceRegistredContext
    {
        ITypeList<IAbpInterceptor> Interceptors { get; }

        Type ImplementationType { get; }
    }
}