#### sprite自定义表单引擎介绍
- .net core研发的自定义表单引擎，采用强大的规则引擎将所有的业务串联起来的，和其他低代码平台是有本质的区别，很多低代码平台不管介绍得有多花哨，界面有多炫酷，本质上还是解决部分复制粘贴的问题。
- 这里的自定义表单引擎绝对不是仅仅解决复制粘贴的问题，目标是完全解放繁琐的CRUD工作。
- sprite自定义表单引擎设计的目的不是给最终客户使用的，而是用于给软件研发的企业降低企业成本，特别适用于项目比较多的公司以及有资源有想法的想要轻资产起步的初创公司或者个人，也特别适用于一些外包的项目，适用于各种ToB的项目，因为能够切实的省去软件研发的绝大部分成本。另外，ToC的项目不一定适合。
- 最佳实践：请结合体验网站和wiki文档实践操作。


#### 软件架构
- 架构设计参考：https://www.cnblogs.com/spritekuang/p/14032899.html
- 表单应用案例：https://www.cnblogs.com/spritekuang/p/14970986.html
- 工作流自定义表单应用案例：https://www.cnblogs.com/spritekuang/p/14970992.html
- 开源地址：https://gitee.com/kuangqifu/sprite
- **体验地址，（首次加载可能有点慢，用的阿里云最差的服务器）** ：http://47.108.141.193:8031  **体验网站说明文档：** https://gitee.com/kuangqifu/sprite/wikis/pages?sort_id=5747937&doc_id=2778937
-   **Wiki文档地址** ：https://gitee.com/kuangqifu/sprite/wikis/pages
- 自定义表单系列文章地址：https://www.cnblogs.com/spritekuang/
- 流程引擎文章地址（采用WWF开发，已过时，已改用Elsa实现）：https://www.cnblogs.com/spritekuang/category/834975.html
- QQ：523477776

#### 体验网站技术清单

- 服务器采用阿里云1核2G，Linux8，Docker部署
- 后端采用的是Abp Vnext，net5，自定义表单部分采用自己封装的一套DDD开发框架，流程引擎采用Elsa
- 前端采用Vue2.X开发，前端框架及控件采用vue ant design实现，前端控件参考网站：https://2x.antdv.com/components/overview/
- 数据库采用Mysql
- 文件存储采用fasfdfs
- 缓存采用Redis+项目内存
- 消息队列采用Rabbitmq
- Job调度采用quartz
- Excel导入导出采用NOPI
- 流程跟踪图采用jsplumb


#### 使用说明
1.  VS打开项目文件 > 03_form\CK.Sprite.Form\CK.Sprite.Form.sln
2.  直接运行项目，数据库连接信息已经在配置文件appsettings.json里面，可用其他数据库连接工具直接打开（外网公共的数据库资源，请不要乱操作数据库）
3.  登录，体验地址，F12进入调试模式，参照各个表单测试表单业务模块调用相关接口 http://47.108.141.193:8031

#### 特殊说明
1. 自定义表单的所有后端功能都已经包含进来了，可以参照体验地址调用相关接口理解这部分设计，前端部分虽然暂时没有开源，但后端的功能是完全支持的，应用到真实项目，后端部分的开发工作完全可以省去很大部分。
1. 自定义表单引擎前端部分也是非常重要且复杂的，不要用Angular来实现，限制太多，因为与我这边的开发框架强依赖，剥离出来要另外花些时间，暂时没有开源出来，后续看情况是否开源，同时也在计划用uniapp实现一套自定义表单。
1. 流程引擎最开始采用WWF开发，现在采用Elsa全部重新开发，比市面上绝大多数流程引擎更加轻量且实现更强大功能，暂时不开源，流程引擎这块自己理解还是比较深刻，对这块感觉兴趣可私下交流。

