﻿using CK.Sprite.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Sprite.JobCore
{
    /// <summary>
    /// Job配置
    /// </summary>
    [Dapper.Contrib.Extensions.Table("JobExecLogs")]
    public class JobExecLog : GuidEntityBase
    {
        /// <summary>
        /// Job配置Id
        /// </summary>
        public Guid JobConfigId { get; set; }

        /// <summary>
        /// Job分组，为空时，添加到默认组
        /// </summary>
        public string JobGroup { get; set; }
        /// <summary>
        /// job描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 触发器类型,Simple = 1,Cron = 2,At = 3
        /// </summary>
        public ETriggerType TriggerType { get; set; }
        /// <summary>
        /// Job名称
        /// </summary>
        public string JobName { get; set; }
        /// <summary>
        /// job执行参数
        /// </summary>
        public string Params { get; set; }
        /// <summary>
        /// job执行类型
        /// </summary>
        public EJobExecType JobExecType { get; set; }
        /// <summary>
        /// 用地址(api时为url，reflect时为类定义信息)
        /// </summary>
        public string ExecLocation { get; set; }
        /// <summary>
        /// Job执行开始时间
        /// </summary>
        public DateTime ExecStartTime { get; set; }
        /// <summary>
        /// Job执行结束时间
        /// </summary>
        public DateTime? ExecEndTime { get; set; }
        /// <summary>
        /// Job执行时长(秒)
        /// </summary>
        public double? ExecDuration { get; set; }
        /// <summary>
        /// Job执行状态
        /// </summary>
        public EExecType ExecType { get; set; }
        /// <summary>
        /// Job执行消息
        /// </summary>
        public string ExecMsg { get; set; }
    }
}
