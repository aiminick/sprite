﻿using CK.Sprite.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Sprite.JobCore
{
    /// <summary>
    /// Job配置Dto
    /// </summary>
    public class JobConfigDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Job分组，为空时，添加到默认组
        /// </summary>
        public string JobGroup { get; set; }
        /// <summary>
        /// job描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 触发器类型
        /// </summary>
        public ETriggerType TriggerType { get; set; }
        /// <summary>
        /// 触发器开始时间，触发器类型为At时，必须指定
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 触发器结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// Cron触发器配置
        /// </summary>
        public string CronConfig { get; set; }
        /// <summary>
        /// 简单触发器，循环单位
        /// </summary>
        public EIntervalUnit? SimpleIntervalUnit { get; set; }
        /// <summary>
        /// 简单触发器，循环值
        /// </summary>
        public int? SimpleIntervalValue { get; set; }
        /// <summary>
        /// 最多执行次数，不设置时，为一直执行
        /// </summary>
        public int? ExecCount { get; set; }
        /// <summary>
        /// 优先级(同一时间执行时生效)
        /// </summary>
        public int? Priority { get; set; }
        /// <summary>
        /// 是否激活
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// 假期配置Id
        /// </summary>
        public Guid? HolidayCalendarId { get; set; }
        /// <summary>
        /// Job名称
        /// </summary>
        public string JobName { get; set; }
        /// <summary>
        /// job执行参数
        /// </summary>
        public string Params { get; set; }
        /// <summary>
        /// job执行类型
        /// </summary>
        public EJobExecType JobExecType { get; set; }
        /// <summary>
        /// 用地址(api时为url，reflect时为类定义信息)
        /// </summary>
        public string ExecLocation { get; set; }
    }

    public class GetJobConfigsInput : PagedDto
    {
        /// <summary>
        /// 通用查询条件
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// 触发器类型,Simple = 1,Cron = 2,At = 3
        /// </summary>
        public ETriggerType? TriggerType { get; set; }

        /// <summary>
        /// job执行类型Api = 1,Reflect = 2
        /// </summary>
        public EJobExecType? JobExecType { get; set; }

        /// <summary>
        /// 是否激活
        /// </summary>
        public bool? IsActive { get; set; }
    }

    public class GetJobExecLogsInput : PagedDto
    {
        /// <summary>
        /// 通用查询条件
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// 触发器类型,Simple = 1,Cron = 2,At = 3
        /// </summary>
        public ETriggerType? TriggerType { get; set; }

        /// <summary>
        /// job执行类型Api = 1,Reflect = 2
        /// </summary>
        public EJobExecType? JobExecType { get; set; }

        /// <summary>
        /// 执行状态，执行中 = 1,执行成功 = 2,执行失败 = 3
        /// </summary>
        public EExecType? ExecType { get; set; }

        /// <summary>
        /// 执行时间查询开始
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 执行时间查询结束
        /// </summary>
        public DateTime? EndTime { get; set; }
    }
}
