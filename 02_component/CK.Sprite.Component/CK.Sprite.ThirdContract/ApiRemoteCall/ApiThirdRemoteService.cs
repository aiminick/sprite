using System.Collections.Generic;
using System.Linq;
using CK.Sprite.Cache;
using CK.Sprite.ThirdContract;
using CK.Sprite.CrossCutting;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public class ApiThirdDeptRemoteService : LazyService, IRemoteSpriteDeptService
    {
        public SpriteConfig _callHttpConfig => LazyGetRequiredService(ref callHttpConfig).Value;
        private IOptions<SpriteConfig> callHttpConfig;

        public List<SpriteDept> GetAll()
        {
            return AsyncHelper.RunSync<List<SpriteDept>>(async () =>
            {
                using (var client = new HttpClient())
                {
                    var url = $"{_callHttpConfig.IdentityUrl}api/identitycache/thirdRemotes/GetAllSpriteDept";

                    var response = await client.GetAsync(
                        url
                    );

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<List<SpriteDept>>(responseContent);
                    }
                    else
                    {
                        throw new Exception("获取数据失败");
                    }
                }
            });
        }
    }

    public class ApiThirdUserRemoteService : LazyService, IRemoteSpriteUserService
    {
        public SpriteConfig _callHttpConfig => LazyGetRequiredService(ref callHttpConfig).Value;
        private IOptions<SpriteConfig> callHttpConfig;

        public List<SpriteUser> GetAll()
        {
            return AsyncHelper.RunSync<List<SpriteUser>>(async () =>
            {
                using (var client = new HttpClient())
                {
                    var url = $"{_callHttpConfig.IdentityUrl}api/identitycache/thirdRemotes/GetAllSpriteUser";

                    var response = await client.GetAsync(
                        url
                    );

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<List<SpriteUser>>(responseContent);
                    }
                    else
                    {
                        throw new Exception("获取数据失败");
                    }
                }
            });
        }
    }

    public class ApiThirdRoleRemoteService : LazyService, IRemoteSpriteRoleService
    {
        public SpriteConfig _callHttpConfig => LazyGetRequiredService(ref callHttpConfig).Value;
        private IOptions<SpriteConfig> callHttpConfig;

        public List<SpriteRole> GetAll()
        {
            return AsyncHelper.RunSync<List<SpriteRole>>(async () =>
            {
                using (var client = new HttpClient())
                {
                    var url = $"{_callHttpConfig.IdentityUrl}api/identitycache/thirdRemotes/GetAllSpriteRole";

                    var response = await client.GetAsync(
                        url
                    );

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<List<SpriteRole>>(responseContent);
                    }
                    else
                    {
                        throw new Exception("获取数据失败");
                    }
                }
            });
        }
    }

    public class ApiThirdUserRoleRemoteService : LazyService, IRemoteSpriteUserRoleService
    {
        public SpriteConfig _callHttpConfig => LazyGetRequiredService(ref callHttpConfig).Value;
        private IOptions<SpriteConfig> callHttpConfig;

        public List<SpriteUserRole> GetAll()
        {
            return AsyncHelper.RunSync<List<SpriteUserRole>>(async () =>
            {
                using (var client = new HttpClient())
                {
                    var url = $"{_callHttpConfig.IdentityUrl}api/identitycache/thirdRemotes/GetAllSpriteUserRole";

                    var response = await client.GetAsync(
                        url
                    );

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<List<SpriteUserRole>>(responseContent);
                    }
                    else
                    {
                        throw new Exception("获取数据失败");
                    }
                }
            });
        }
    }
}
