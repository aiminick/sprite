﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    /// <summary>
    /// 第三方服务接口
    /// </summary>
    public interface IThirdService
    {
        /// <summary>
        /// 根据用户Id集合获取用户信息
        /// </summary>
        /// <param name="ids">用户Id集合</param>
        /// <returns></returns>
        Task<List<SpriteUser>> GetSpriteUsersByIds(IEnumerable<string> ids);

        /// <summary>
        /// 根据用户Id获取用户信息
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        Task<SpriteUser> GetSpriteUser(string id);

        /// <summary>
        /// 工作流获取用户信息
        /// </summary>
        /// <param name="thirdUserInput">获取信息输入</param>
        /// <returns></returns>
        Task<List<SpriteUser>> GetSpriteUsers(GetThirdUserInput thirdUserInput);
    }
}

