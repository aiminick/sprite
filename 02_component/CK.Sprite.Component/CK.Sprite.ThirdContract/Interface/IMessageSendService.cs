﻿using CK.Sprite.ThirdContract.Model;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public interface IMessageSendService
    {
        Task<string> AddMessageSend(CreateMessageSendDto createMessageSendDto);
    }
}
