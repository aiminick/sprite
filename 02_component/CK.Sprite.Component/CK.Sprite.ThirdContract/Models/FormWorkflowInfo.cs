﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    /// <summary>
    /// 表单关联查询流程信息
    /// </summary>
    public class FormWorkflowInfo
    {
        public string InstanceId { get; set; }

        public string Status { get; set; }

        /// <summary>
        /// 当前审批活动
        /// </summary>
        public string CurrentActivities { get; set; }

        /// <summary>
        /// 流程实例名称
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? StartedAt { get; set; }

        public DateTime? FinishedAt { get; set; }
    }
}
