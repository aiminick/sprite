﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    /// <summary>
    /// 部门信息
    /// </summary>
    public class SpriteDept
    {
        /// <summary>
        /// 部门Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 父级部门Id
        /// </summary>
        public string PId { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 部门路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Code路径
        /// </summary>
        public string TreeCode { get; set; }
    }
}
