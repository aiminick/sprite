﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Sprite.ThirdContract.Model
{
    /// <summary>
    /// 消息发送新建模型
    /// </summary>
    [Serializable]
    public class CreateMessageSendDto
    {
        /// <summary>
        /// 主题
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        public ELevel Level { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        public string MessageType { get; set; }
        /// <summary>
        /// 模板内容
        /// </summary>
        public string TemplateContent { get; set; }
        /// <summary>
        /// 平台
        /// </summary>
        public EPlatform Platform { get; set; }
        /// <summary>
        /// 发送时间类型设置
        /// </summary>
        public ESendTimeType SendTimeType { get; set; }
        /// <summary>
        /// 发送时间类型设置值
        /// </summary>
        public string SendTimeValue { get; set; }
        /// <summary>
        /// Web端点击消息跳转
        /// </summary>
        public string WebUrl { get; set; }
        /// <summary>
        /// 业务信息存储
        /// </summary>
        public string BusinessActionContent { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public Guid Creator { get; set; }
        /// <summary>
        /// 接受者列表
        /// </summary>
        public List<CreateReceiverDto> CreateReceiverDtos { get; set; }
        /// <summary>
        /// 消息发送参数
        /// </summary>
        public string MessageSendParam { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CreateReceiverDto
    {
        /// <summary>
        /// 接受者类型
        /// </summary>
        public EReceiveType ReceiveType { get; set; }
        /// <summary>
        /// 接收者值
        /// </summary>
        public string ReceiveValues { get; set; }
        /// <summary>
        /// 子分类
        /// </summary>
        public string SubCategory { get; set; }
    }

    /// <summary>
    /// 等级
    /// </summary>
    public enum ELevel
    {
        一般 = 1,
        重要 = 2,
        紧急 = 3,
        重要紧急 = 4
    }

    /// <summary>
    /// 发送时间类型设置
    /// </summary>
    public enum ESendTimeType
    {
        及时 = 1,
        定时 = 2,
        周期 = 3
    }

    /// <summary>
    /// 平台类型
    /// </summary>
    public enum EPlatform
    {
        基础平台 = 1,
        流程审批 = 2
    }

    /// <summary>
    /// 接收者类型
    /// </summary>
    public enum EReceiveType
    {
        系统角色 = 1,
        部门 = 2,
        部门及下级部门 = 3,
        个人 = 4
    }
}
