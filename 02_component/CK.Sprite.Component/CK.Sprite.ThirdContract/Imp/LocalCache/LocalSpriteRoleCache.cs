﻿using CK.Sprite.Cache;
using CK.Sprite.CrossCutting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public class LocalSpriteRoleCache : BaseLocalCache<SpriteRole>, ISpriteRoleCache
    {
        public IRemoteSpriteRoleService _remoteSpriteRoleService => LazyGetRequiredService(ref remoteSpriteRoleService);
        private IRemoteSpriteRoleService remoteSpriteRoleService;

        public override string CacheKey => ThirdConsts.SpriteRoleCacheKey;

        public override List<SpriteRole> GetAll(string applicationCode = null)
        {
            if (!LocalCacheContainer.IsEnabledLocalCache)
            {
                return _remoteSpriteRoleService.GetAll();
            }
            else
            {
                return (List<SpriteRole>)LocalCacheContainer.Get($"{ThirdConsts.ThirdCachePreKey}-{CacheKey}", key =>
                {
                    return _remoteSpriteRoleService.GetAll();
                });
            }
        }

        public override Dictionary<Guid, SpriteRole> GetAllDict(string applicationCode = null)
        {
            return null;
        }
    }
}
