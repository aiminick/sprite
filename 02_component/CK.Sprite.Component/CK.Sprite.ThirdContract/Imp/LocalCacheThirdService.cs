﻿using CK.Sprite.CrossCutting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public class LocalCacheThirdService : LazyService, IThirdService
    {
        public ISpriteUserCache _spriteUserCache => LazyGetRequiredService(ref spriteUserCache);
        private ISpriteUserCache spriteUserCache;

        public ISpriteUserRoleCache _spriteUserRoleCache => LazyGetRequiredService(ref spriteUserRoleCache);
        private ISpriteUserRoleCache spriteUserRoleCache;

        public ISpriteDeptCache _spriteDeptCache => LazyGetRequiredService(ref spriteDeptCache);
        private ISpriteDeptCache spriteDeptCache;

        public async Task<SpriteUser> GetSpriteUser(string id)
        {
            var result = await _spriteUserCache.GetById(id);
            return result;
        }

        public async Task<List<SpriteUser>> GetSpriteUsers(GetThirdUserInput thirdUserInput)
        {
            var allUserIds = new List<string>();
            if (thirdUserInput.UserIds != null && thirdUserInput.UserIds.Count > 0)
            {
                allUserIds.AddRange(thirdUserInput.UserIds);
            }

            var limitUser = await _spriteUserCache.GetById(thirdUserInput.LimitUserId);

            if (thirdUserInput.ThirdUserRoleDetailInputs != null)
            {
                foreach (var thirdUserRoleDetailInput in thirdUserInput.ThirdUserRoleDetailInputs)
                {
                    if (string.IsNullOrEmpty(thirdUserRoleDetailInput.DeptId)) // 不限制部门
                    {
                        var tempUserIds = await _spriteUserRoleCache.GetUserIdsByRoleId(thirdUserRoleDetailInput.RoleId);
                        allUserIds.AddRange(tempUserIds);
                    }
                    else
                    {
                        string deptId = "";
                        if (thirdUserRoleDetailInput.DeptId == "-1") // 限制本部门
                        {
                            deptId = limitUser.MainDeptId;
                        }
                        else if (thirdUserRoleDetailInput.DeptId == "-2") // 限制上级部门
                        {
                            deptId = (await _spriteDeptCache.GetById(limitUser.MainDeptId)).PId;
                        }
                        else // 其他选择的部门
                        {
                            deptId = thirdUserRoleDetailInput.DeptId;
                        }

                        var tempUserIds = await _spriteUserRoleCache.GetUserIdsByRoleId(thirdUserRoleDetailInput.RoleId);
                        tempUserIds = (await _spriteUserCache.GetByIdsAndDeptId(tempUserIds, deptId)).Select(r => r.Id).ToList();
                        allUserIds.AddRange(tempUserIds);
                    }
                }
            }
            if (allUserIds.Count > 0)
            {
                allUserIds = allUserIds.Distinct().ToList();
                var result = await _spriteUserCache.GetByIds(allUserIds);
                return result;
            }
            else
            {
                return new List<SpriteUser>();
            }
        }

        public async Task<List<SpriteUser>> GetSpriteUsersByIds(IEnumerable<string> ids)
        {
            var result = await _spriteUserCache.GetByIds(ids);
            return result;
        }
    }
}

