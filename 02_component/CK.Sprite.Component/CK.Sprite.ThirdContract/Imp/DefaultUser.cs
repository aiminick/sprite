﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    /// <summary>
    /// 当前租户信息
    /// </summary>
    public class DefaultCurrentUser : ICurrentUser
    {
        public string UserId => "503e4458-8998-16e9-8243-39f586ee75ce";

        public string UserName => "admin";
    }
}
