﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using System.Web;

namespace CK.Sprite.MessageComponent.Test
{
    /// <summary>
    /// Excel导入导出测试
    /// </summary>
    [ApiController]
    [Area("excel")]
    [ControllerName("ExcelTest")]
    [Route("api/excel/ExcelTest")]
    public class EmailController : Controller
    {
        private readonly ISmtpService _smtpService;

        public EmailController(ISmtpService smtpService)
        {
            _smtpService = smtpService;
        }

        [HttpPost]
        [Route("SendEmail")]
        public async Task<SendResultEntity> SendEmail(MailEntity mailEntity)
        {
            return await _smtpService.SendEmailAsync(mailEntity);
        }
    }
}
