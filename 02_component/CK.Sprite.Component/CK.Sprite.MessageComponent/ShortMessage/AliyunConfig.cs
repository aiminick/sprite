﻿namespace CK.Sprite.MessageComponent
{
    public class AliyunConfig
    {
        public string AccessKeyID { get; set; }
        public string AccessKeySecret { get; set; }
        public string SignName { get; set; }
        public string MessageTemplateCode { get; set; }
    }
}
