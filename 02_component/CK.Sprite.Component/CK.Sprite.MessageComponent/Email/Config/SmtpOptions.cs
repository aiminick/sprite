using MailKit.Security;

namespace CK.Sprite.MessageComponent
{
    public class SmtpOptions
    {
        public string DefaultSender { get; set; }
        public string DefaultSenderAddress { get; set; }
        public string Host { get; set; }
        public int Port { get; set; } = 25;
        public SecureSocketOptions SecureSocketOptions { get; set; } = SecureSocketOptions.Auto;
        public bool RequireCredentials { get; set; } = true;
        public bool UseDefaultCredentials { get; set; } = false;
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}