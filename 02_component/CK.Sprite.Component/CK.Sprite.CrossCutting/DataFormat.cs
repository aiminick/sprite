﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CK.Sprite.CrossCutting
{
    /// <summary>
    /// 数值转换显示
    /// </summary>
    public static class DataFormat
    {
        /// <summary>
        /// 转换文件大小为适合单位的显示
        /// </summary>
        /// <param name="fileLength">原文件大小,单位字节</param>
        /// <param name="decimals">保留小数位数</param>
        /// <returns></returns>
        public static string GetFileLength(this long fileLength, int decimals = 2)
        {
            decimal length = decimal.Parse(fileLength.ToString());
            if (length < 1024)
            {
                return $"{length}B";
            }

            if (length < 1024 * 1024)
            {
                return $"{decimal.Round(length / 1024 , decimals)}KB";
            }

            if (length < 1024 * 1024 * 1024)
            {
                return $"{decimal.Round(length / (1024 * 1024), decimals)}MB";
            }

            return $"{decimal.Round(length / (1024 * 1024 * 1024), decimals)}GB";
        }

        public static void GetWeekDateByDate(DateTime date, out DateTime beginDate,out DateTime endDate)
        {
            var day = date.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)date.DayOfWeek;
            beginDate = date.AddDays(1 - day).Date;
            endDate = beginDate.AddDays(7).AddMilliseconds(-1);
        }
        public static void GetMonthDateByDate(DateTime date, out DateTime beginDate, out DateTime endDate)
        {
            beginDate = new DateTime(date.Year, date.Month, 1);
            endDate = beginDate.AddMonths(1).AddMilliseconds(-1);
        }

        public static void GetYearDateByDate(DateTime date, out DateTime beginDate, out DateTime endDate)
        {
            beginDate = new DateTime(date.Year, date.Month, 1).AddMonths(-11);
            endDate = new DateTime(date.Year, date.Month, 1).AddMonths(1).AddMilliseconds(-1);
		}
		
        public static string GetWeekName(DateTime date, CultureInfo culture = null)
        {
            if (null == culture)
            {
                culture = new CultureInfo("zh-CN");
            }

            GetWeekDateByDate(date, out DateTime beginDate, out DateTime endDate);

            var weekOfYear = culture.Calendar.GetWeekOfYear(beginDate, culture.DateTimeFormat.CalendarWeekRule, culture.DateTimeFormat.FirstDayOfWeek);
            return $"{beginDate.Year}年第{weekOfYear}周({beginDate.Year}.{beginDate.Month}.{beginDate.Day}-{endDate.Year}.{endDate.Month}.{endDate.Day})";
        }

        public static string GetWeekName(this DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return "周一";
                case DayOfWeek.Tuesday:
                    return "周二";
                case DayOfWeek.Wednesday:
                    return "周三";
                case DayOfWeek.Thursday:
                    return "周四";
                case DayOfWeek.Friday:
                    return "周五";
                case DayOfWeek.Saturday:
                    return "周六";
                case DayOfWeek.Sunday:
                    return "周日";
                default:
                    return "未知";
            }
        }
    }
}
