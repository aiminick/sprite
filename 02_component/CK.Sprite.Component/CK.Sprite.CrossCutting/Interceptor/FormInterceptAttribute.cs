﻿using System;

namespace CK.Sprite.CrossCutting
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property)]
    public class FormInterceptAttribute : Attribute
    {

    }
}
