﻿namespace CK.Sprite.CrossCutting
{
    /// <summary>
    /// 调用自定义表单方法验证表单视图是否过期
    /// </summary>
    public interface IFormViewValidator
    {
        /// <summary>
        /// 验证视图表单是否是最新的数据
        /// </summary>
        /// <param name="relationInfos">header信息，参见RelationInfoInputs实体，List集合</param>
        /// <returns>验证是否通过</returns>
        bool CheckIsLastVersion(string relationInfos);
    }
}
