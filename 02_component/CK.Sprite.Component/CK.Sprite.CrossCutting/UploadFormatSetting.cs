﻿using System.Collections.Generic;

namespace CK.Sprite.CrossCutting
{
    public class UploadFormatSettingOptions
    {
        public List<UploadFormatSetting> UploadFormatSettings { get; set; }
    }

    /// <summary>
    /// 上传文件格式
    /// </summary>
    public class UploadFormatSetting
    {
        /// <summary>
        /// 文件格式类别
        /// </summary>
        public string FormateCategory { get; set; }

        /// <summary>
        /// 白名单/黑名单(白名单=0,黑名单=1)
        /// </summary>
        public UploadSettingType SettingType { get; set; }

        /// <summary>
        /// 白名单设置
        /// </summary>
        public List<string> WhiteList { get; set; } = new List<string>();


        /// <summary>
        /// 黑名单设置
        /// </summary>
        public List<string> BlackList { get; set; } = new List<string>();
    }

    public enum UploadSettingType
    {
        WhiteList = 0,

        BlackList = 1
    }
}
