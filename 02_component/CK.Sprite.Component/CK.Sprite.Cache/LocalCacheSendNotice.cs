﻿using System.Collections.Generic;

namespace CK.Sprite.Cache
{
    /// <summary>
    /// 本地缓存容器通知服务
    /// </summary>
    public class LocalCacheSendNotice : ICacheSendNotice
    {
        public void SendClearCache(string key)
        {
            ReceiveCacheNotice.ReceiveClearCache(key);
        }

        public void SendClearCaches(List<string> keys)
        {
            ReceiveCacheNotice.ReceiveClearCaches(keys);
        }
    }
}
