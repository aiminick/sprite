﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using Aliyun.OSS.NetCore.Model;

namespace Aliyun.OSS.NetCore
{
    /// <summary>
    /// The result class of the operation to get bucket's storage capacity.
    /// </summary>
    public class GetBucketStorageCapacityResult : GenericResult
    {
        /// <summary>
        /// The bucket storage capacity.
        /// </summary>
        public long StorageCapacity { get; internal set; }
    }
}
