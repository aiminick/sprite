﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using System.IO;
using Aliyun.OSS.NetCore.Common.Communication;
using Aliyun.OSS.NetCore.Model;

namespace Aliyun.OSS.NetCore.Transform
{
    internal class GetBucketRequestPaymentResultDeserializer : ResponseDeserializer<GetBucketRequestPaymentResult, RequestPaymentConfiguration>
    {
        public GetBucketRequestPaymentResultDeserializer(IDeserializer<Stream, RequestPaymentConfiguration> contentDeserializer)
            : base(contentDeserializer)
        { }

        public override GetBucketRequestPaymentResult Deserialize(ServiceResponse xmlStream)
        {
            GetBucketRequestPaymentResult result = new GetBucketRequestPaymentResult();

            var mode = ContentDeserializer.Deserialize(xmlStream.Content);

            result.Payer = mode.Payer;

            this.DeserializeGeneric(xmlStream, result);
            return result;
        }
    }
}

