﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using System.IO;
using Aliyun.OSS.NetCore.Common.Communication;
using Aliyun.OSS.NetCore.Model;
using Aliyun.OSS.NetCore.Util;

namespace Aliyun.OSS.NetCore.Transform
{
    internal class SetBucketEncryptionRequestSerializer : RequestSerializer<SetBucketEncryptionRequest, ServerSideEncryptionRule>
    {
        public SetBucketEncryptionRequestSerializer(ISerializer<ServerSideEncryptionRule, Stream> contentSerializer)
            : base(contentSerializer)
        {
        }

        public override Stream Serialize(SetBucketEncryptionRequest request)
        {
            var model = new ServerSideEncryptionRule()
            {
                ApplyServerSideEncryptionByDefault = new ServerSideEncryptionRule.ApplyServerSideEncryptionByDefaultModel()
            };

            model.ApplyServerSideEncryptionByDefault.SSEAlgorithm = request.SSEAlgorithm;
            model.ApplyServerSideEncryptionByDefault.KMSMasterKeyID = request.KMSMasterKeyID;
            return ContentSerializer.Serialize(model);
        }
    }
}

