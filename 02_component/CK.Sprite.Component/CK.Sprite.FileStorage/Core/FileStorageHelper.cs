﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CK.Sprite.FileStorage.Application;

namespace CK.Sprite.FileStorage
{
    public static class FileStorageHelper
    {        
        private static IFileStoreManager _fileStoreManager;
        private static ConcurrentDictionary<int, StorageProvider> StorageProviderManager { get; set;}
        private static int OtherStoreDesignPicturePathId = 1;
        static FileStorageHelper()
        {
            StorageProviderManager = new ConcurrentDictionary<int, StorageProvider>();
        }

        public static void SetFileStoreManager(IFileStoreManager fileStoreManager)
        {
            _fileStoreManager = fileStoreManager;
        }

        public static async Task<StorageProvider> GetStorageProviderByIdAsync(int fileStoreId)
        {
            if (!StorageProviderManager.ContainsKey(fileStoreId))
            {
                var allStoreInfos = await _fileStoreManager.GetStoreInfos();
                foreach (var storeInfo in allStoreInfos)
                {
                    if (!StorageProviderManager.ContainsKey(storeInfo.Id))
                    {
                        switch (storeInfo.StoreType)
                        {
                            case EStoreType.FastDfs:
                                StorageProviderManager.TryAdd(storeInfo.Id, new FastDfsStorageProvider(new FastDfsOptions()
                                {
                                    StoreType = storeInfo.StoreType,
                                    EnableLocalStorageProvider = false,
                                    Trackers = storeInfo.ServiceAddress.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries),
                                    HttpDomain = storeInfo.HttpDomain
                                }));
                                break;
                            case EStoreType.OSS:
                                StorageProviderManager.TryAdd(storeInfo.Id, new OSSStorageProvider(new OSSStorageOptions
                                {
                                    StoreType = storeInfo.StoreType,
                                    HttpDomain = storeInfo.HttpDomain,
                                    ServerAddress = storeInfo.ServiceAddress,
                                    AccessKeyId = storeInfo.AccessKeyId,
                                    AccessKeySecret = storeInfo.AccessKeySecret,
                                    Bucket = storeInfo.Bucket
                                }));
                                break;
                            case EStoreType.LocalHost:
                                StorageProviderManager.TryAdd(storeInfo.Id, new LocalStorageProvider(new FileStorgeOptions()
                                {
                                    StoreType = storeInfo.StoreType,
                                    HttpDomain = storeInfo.HttpDomain
                                }));
                                break;
                        }
                    }
                }
            }

            return StorageProviderManager[fileStoreId];
        }

        public static void TrySetStorageProvider(List<StoreInfo> storeInfos)
        {
            foreach (var storeInfo in storeInfos)
            {
                if (!StorageProviderManager.ContainsKey(storeInfo.Id))
                {
                    switch (storeInfo.StoreType)
                    {
                        case EStoreType.FastDfs:
                            StorageProviderManager.TryAdd(storeInfo.Id, new FastDfsStorageProvider(new FastDfsOptions()
                            {
                                EnableLocalStorageProvider = false,
                                Trackers = storeInfo.ServiceAddress.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries),
                                HttpDomain = storeInfo.HttpDomain
                            }));
                            break;
                        case EStoreType.OSS:
                            StorageProviderManager.TryAdd(storeInfo.Id, new OSSStorageProvider(new OSSStorageOptions
                            {
                                HttpDomain = storeInfo.HttpDomain,
                                ServerAddress = storeInfo.ServiceAddress,
                                AccessKeyId = storeInfo.AccessKeyId,
                                AccessKeySecret = storeInfo.AccessKeySecret,
                                Bucket = storeInfo.Bucket
                            }));
                            break;
                    }
                }
            }
        }
    }
}

