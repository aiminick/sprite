﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Builder;
using System.Net.WebSockets;

namespace CK.Sprite.FileStorage
{
	public static class LocalFileStoreServiceExtensions
    {
        public static IServiceCollection AddLocalFileStore(this IServiceCollection services)
        {
            return services.AddSingleton(typeof(IFileStoreManager), typeof(LocalFileStoreManager));
        }
    }
}