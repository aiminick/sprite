﻿
using Microsoft.Extensions.Options;
using System.IO;
using System.Threading.Tasks;
using CK.Sprite.FileStorage.Application;

namespace CK.Sprite.FileStorage
{
    /// <summary>
    /// Storage provider base
    /// </summary>
    public abstract class StorageProvider
    {
        public readonly FileStorgeOptions Options;

        public const string UrlSeparator = "/";
        protected StorageProvider(FileStorgeOptions options)
        {
            Options = options;
        }

        /// <summary>
        /// store local file
        /// </summary>
        /// <param name="fileName">absolute path of the local file</param>
        /// <param name="ext">file extension without '.', if null, get it from the fileName.</param>
        /// <param name="groupName">storage group, defualt is null</param>
        /// <returns>0: file absolute url; 1: file id</returns>
        public abstract Task<string[]> StoreAsync(string fileName, string ext, string groupName = null);

        /// <summary>
        /// store local file
        /// </summary>
        /// <param name="fileStream">file stream</param>
        /// <param name="ext">file extension without '.', can not be null</param>
        /// <param name="groupName">storage group, defualt is null</param>
        /// <returns>0: file absolute url; 1: file id</returns>
        public abstract Task<string[]> StoreAsync(Stream fileStream, string ext, string groupName = null);

        /// <summary>
        /// download file from storage server
        /// </summary>
        /// <param name="remoteFilename">filename on storage server</param>
        /// <param name="localFilename">filename on local</param>
        /// <param name="groupName">the group name of storage server</param>
        /// <returns>0 success, return none zero errno if fail</returns>
        public abstract Task<int> DownloadFileAsync(string remoteFilename, string localFilename, string groupName = null);

        /// <summary>
        /// delete file from storage server
        /// </summary>
        /// <param name="remoteFilename">filename on storage server</param>
        /// <param name="groupName">the group name of storage server</param>
        /// <returns>0 for success, none zero for fail (error code)</returns>
        public abstract Task<int> DeleteFileAsync(string remoteFilename, string groupName = null);

        /// <summary>
        /// 计算浏览器下载地址
        /// </summary>
        /// <param name="serverPath">服务存储地址</param>
        /// <param name="fileName">下载文件名称</param>
        /// <returns></returns>
        public abstract string CalculateDownServerPath(string serverPath, string fileName);

        /// <summary>
        /// return the statistics of storage
        /// </summary>
        /// <returns></returns>
        //public virtual async Task<object> StatisticAsync()
        //{
        //    return await Task.Run(() => new
        //    {
        //        Provider = this.Options.EnableLocalStorageProvider ? "Local" : "FastDFS"
        //    });
        //}


    }
}