﻿namespace FastDFS.Client
{
    public class StorageServer : FdfsServer
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ip">the ip address of storage server</param>
        /// <param name="port">the port of storage server</param>
        /// <param name="storePath">he store path index on the storage server</param>
        public StorageServer(string ip, int port, int storePath)
           :base(ip,port)
        {
            this.StorePathIndex = storePath;
        }
        

        public int StorePathIndex { get; private set; }
        
    }
}
