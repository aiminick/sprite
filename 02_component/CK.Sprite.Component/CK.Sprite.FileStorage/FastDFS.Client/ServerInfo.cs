﻿using System.Net.Sockets;

namespace FastDFS.Client
{
    public class ServerInfo
    {

        public ServerInfo(string ipAddr, int port)
        {
            this.IpAddr = ipAddr;
            this.Port = port;
        }

        public string IpAddr { get; private set; }

        public int Port { get; private set; }

        /// <summary>
        /// connect to server
        /// </summary>
        /// <returns>connected TcpClient object</returns>
        public  System.Threading.Tasks.Task<TcpClient> ConnectAsync()
        {
           return  ClientGlobal.GetSocketAsync(this.IpAddr, this.Port);
        }
    }
}
