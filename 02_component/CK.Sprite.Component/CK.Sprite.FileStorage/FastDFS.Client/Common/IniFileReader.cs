﻿using System;
using System.Collections;
using System.IO;

namespace FastDFS.Client
{
  
    public class IniFileReader
    {
        private Hashtable _paramTable;
        private string _confFilename;

        public IniFileReader(string confFilename)
        {
            this._confFilename = confFilename;
            LoadFromFile(confFilename);
        }

        public string GetConfFilename()
        {
            return this._confFilename;
        }

        public string GetStrValue(string name)
        {
            object obj;
            obj = this._paramTable[name];
            if (obj == null)
            {
                return null;
            }

            if (obj is string)
            {
                return (string)obj;
            }

            return (string)((ArrayList)obj)[0];
        }

        public int GetIntValue(string name, int defaultValue)
        {
            string szValue = this.GetStrValue(name);
            if (szValue == null)
            {
                return defaultValue;
            }

            return int.Parse(szValue);
        }

        public bool GetBoolValue(string name, bool defaultValue)
        {
            string szValue = this.GetStrValue(name);
            if (szValue == null)
            {
                return defaultValue;
            }

            return szValue.Equals("yes", StringComparison.OrdinalIgnoreCase) || szValue.Equals("on", StringComparison.OrdinalIgnoreCase) ||
                         szValue.Equals("true", StringComparison.OrdinalIgnoreCase) || szValue.Equals("1", StringComparison.OrdinalIgnoreCase);
        }

        public string[] GetValues(string name)
        {
            object obj;
            string[] values;

            obj = this._paramTable[name];
            if (obj == null)
            {
                return null;
            }

            if (obj is string)
            {
                values = new string[1];
                values[0] = (string)obj;
                return values;
            }

            object[] objs = ((ArrayList)obj).ToArray();
            values = new string[objs.Length];
            Array.Copy(objs, 0, values, 0, objs.Length);
            return values;
        }

        private void LoadFromFile(string confFilename)
        {
            TextReader buffReader;
            string line;
            string[] parts;
            string name;
            string value;
            object obj;
            ArrayList valueList;

            buffReader = new StringReader(File.ReadAllText(confFilename));
            this._paramTable = new Hashtable();

            try
            {
                while ((line = buffReader.ReadLine()) != null)
                {
                    line = line.Replace("\0", "").Trim();
                    if (line.Length == 0 || line[0] == '#')
                    {
                        continue;
                    }

                    parts = line.Split("=".ToCharArray(), 2);
                    if (parts.Length != 2)
                    {
                        continue;
                    }

                    name = parts[0].Replace("\0", "").Trim();
                    value = parts[1].Replace("\0", "").Trim();

                    obj = this._paramTable[name];
                    if (obj == null)
                    {
                        this._paramTable[name] = value;
                    }
                    else if (obj is string)
                    {
                        valueList = new ArrayList();
                        valueList.Add(obj);
                        valueList.Add(value);
                        this._paramTable[name] = valueList;
                    }
                    else
                    {
                        valueList = (ArrayList)obj;
                        valueList.Add(value);
                    }
                }
            }
            finally
            {
                buffReader.Dispose();
            }
        }
    }
}
