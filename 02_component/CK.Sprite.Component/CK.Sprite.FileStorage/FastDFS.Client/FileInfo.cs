﻿using System;

namespace FastDFS.Client
{
    
    public class FileInfo
    {
        public FileInfo(long fileSize, int createTimestamp, int crc32, string sourceIpAddr)
        {
            this.FileSize = fileSize;
            //this.create_timestamp = new DateTime(create_timestamp * 1000L);
            this.CreateTimestamp = ProtoCommon.UnixTimestampToDateTime(createTimestamp);
            this.Crc32 = crc32;
            this.SourceIpAddr = sourceIpAddr;
        }

        public string SourceIpAddr { get; set; }

        public long FileSize { get; set; }

        public DateTime CreateTimestamp { get; set; }

        public int Crc32 { get; set; }

        public override string ToString()
        {
            return "source_ip_addr = " + this.SourceIpAddr + ", " +
                    "file_size = " + this.FileSize + ", " +
                    "create_timestamp = " + this.CreateTimestamp.ToString("yyyy-MM-dd HH:mm:ss") + ", " +
                    "crc32 = " + this.Crc32;
        }
    }
}
