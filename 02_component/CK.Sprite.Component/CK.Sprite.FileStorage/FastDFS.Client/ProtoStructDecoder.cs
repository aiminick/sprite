﻿using System;
using System.IO;

namespace FastDFS.Client
{
    public class ProtoStructDecoder
    {
        public ProtoStructDecoder()
        {
        }

        public T[] Decode<T>(byte[] bs, int fieldsTotalSize) where T : StructBase
        {
            if (bs.Length % fieldsTotalSize != 0)
            {
                throw new IOException("byte array length: " + bs.Length + " is invalid!");
            }

            int count = bs.Length / fieldsTotalSize;
            int offset;
            T[] results = new T[count];

            offset = 0;
            for (int i = 0; i < results.Length; i++)
            {
                results[i] = Activator.CreateInstance<T>();
                results[i].SetFields(bs, offset);
                offset += fieldsTotalSize;
            }

            return results;
        }
    }
}
