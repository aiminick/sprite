﻿using System.IO;

namespace FastDFS.Client
{
    public class DownloadStream : IDownloadCallback
    {
        private Stream _output;
        private long _currentBytes = 0;

        public DownloadStream(Stream output)
        {
            this._output = output;
        }

        public int Recv(long fileSize, byte[] data, int bytes)
        {
            try
            {
                _output.Write(data, 0, bytes);
            }
            catch (IOException)
            {
                return -1;
            }

            _currentBytes += bytes;
            if (this._currentBytes == fileSize)
            {
                this._currentBytes = 0;
            }

            return 0;
        }
    }
}
