﻿namespace CK.Sprite.FileStorage
{
    /// <summary>
    /// configurations of FastDFS client
    /// </summary>
    public class StoreInfo
    {
        /// <summary>
        /// 文档存储Id
        /// </summary>
        public int Id { get; set; }

        public EStoreType StoreType { get; set; }

        /// <summary>
        /// 服务地址
        /// </summary>
        public string ServiceAddress { get; set; }

        /// <summary>
        /// http访问前缀
        /// </summary>
        public string HttpDomain { get; set; } = string.Empty;

        /// <summary>
        /// 阿里云OSS使用,Bucket名称
        /// </summary>
        public string Bucket { get; set; }

        /// <summary>
        /// 服务器密钥Key
        /// </summary>
        public string AccessKeyId { get; set; }

        /// <summary>
        /// 服务器密钥Value
        /// </summary>
        public string AccessKeySecret { get; set; }

    }

    public enum EStoreType
    {
        FastDfs = 1,
        OSS = 2,
        LocalHost = 3
    }
}