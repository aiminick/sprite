﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace CK.Sprite.RabbitMQ.Core
{
    public class Utf8JsonRabbitMqSerializer: IRabbitMqSerializer
    {
        public byte[] Serialize(object obj)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));
        }

        public T Deserialize<T>(byte[] value)
        {
            return JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(value));
        }
    }
}