﻿using System;
using RabbitMQ.Client;

namespace CK.Sprite.RabbitMQ.Core
{
    public interface IConnectionPool : IDisposable
    {
        IConnection Get(string connectionName = null);
    }
}