﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CK.Sprite.Cache.Redis
{
    public static class RedisCacheServiceExtensions
    {
        public static IServiceCollection AddRedisCacheSendNotice(this IServiceCollection services)
        {
            return services.AddSingleton(typeof(ICacheSendNotice), typeof(RedisCacheSendNotice));
        }

        public static void UseRedisCacheSendNotice(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetService<ICacheSendNotice>();
        }
    }
}
