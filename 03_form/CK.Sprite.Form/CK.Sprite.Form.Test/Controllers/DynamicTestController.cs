﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Test.Models;
using Newtonsoft.Json.Linq;
using System.IO.Compression;
using System.IO;
using System.Net;
using System.Collections;
using System.Text;
using Newtonsoft.Json;
using CK.Sprite.Form.Core;
using CK.Sprite.ThirdContract;

namespace CK.Sprite.Form.Test.Controllers
{
    [ApiController]
    [Area("DynamicTest")]
    [ControllerName("DynamicTest")]
    [Route("api/spriteform/dynamictest")]
    public class DynamicTestController : Controller
    {
        private readonly IRuntimeAppService _runtimeAppService;
        private readonly IFormThirdServiceAppService _formThirdServiceAppService;

        public DynamicTestController(IRuntimeAppService runtimeAppService, IFormThirdServiceAppService formThirdServiceAppService)
        {
            _runtimeAppService = runtimeAppService;
            _formThirdServiceAppService = formThirdServiceAppService;
        }

        /// <summary>
        /// {ApplicationCode:"Default",Methods:[{MethodId:Guid1,ParamValues:JObject}]}
        /// 自定义Sql语句注意区分哪些是Update或者Create构造字段，哪些是参数
        /// </summary>
        /// <param name="paramObject"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CallMethodTest")]
        public async Task<object> CallMethodTest(object paramObject)
        {
            return await _formThirdServiceAppService.DoRuntimeMethod(paramObject as JObject);
        }

        [HttpGet]
        [Route("GetCallMethodTest")]
        public object GetCallMethodTest()
        {
            string strCallMethodDatas = @"
{
    'ApplicationCode':'Default',
    'IsTransaction':true,
    'Methods':[
        //{
        //    'ObjectName':'Student',
        //    'MethodId':'Create',
        //    'RuleId':'RuleGuid1',
        //    'ParamValues':
        //        {
        //            'Name':'fangfang2',
        //            'Age':19,
        //            'Birthday':'2000-10-10'
        //        }
        //},
        //{
        //    'ObjectName':'Student',
        //    'MethodId':'Update',
        //    'RuleId':'RuleGuid2',
        //    'ParamValues':
        //        {
        //            'Id':1,
        //            'Name':'kuang',
        //            'Age':21,
        //            'Birthday':'2000-10-10'
        //        }
        //},
        //{
        //    'ObjectName':'Student',
        //    'MethodId':'Delete',
        //    'RuleId':'RuleGuid3',
        //    'ParamValues':
        //        {
        //            'Id':7
        //        }
        //},
        {
            'ObjectName':'Student',
            'MethodId':'Get',
            'RuleId':'RuleGuid4',
            'ParamValues':
                {
                    'Id':1
                }
        },
        //{
        //    'ObjectName':'Student',
        //    'MethodId':'List',
        //    'RuleId':'RuleGuid5'
        //},
        //{
        //    'ObjectName':'Student',
        //    'MethodId':'UpdateWhere',
        //    'RuleId':'RuleGuid6',
        //    'ParamValues':
        //        {
        //            'Age':31,
        //            'Birthday':'2020-07-19'
        //        },
        //    'SqlWheres':
        //        [
        //            {
        //                'Field':'Name',
        //                'ConditionType':2,
        //                'Value':'fang'
        //            },
        //            {
        //                'Field':'Age',
        //                'ConditionType':6,
        //                'Value':18
        //            }
        //        ]
        //},
        //{
        //    'ObjectName':'Student',
        //    'MethodId':'ListWhere',
        //    'RuleId':'RuleGuid7',
        //    'SqlWheres':
        //        [
        //            {
        //                'Field':'Name',
        //                'ConditionType':2,
        //                'Value':'fang'
        //            },
        //            {
        //                'Field':'Age',
        //                'ConditionType':6,
        //                'Value':18
        //            }
        //        ]
        //},
        {
            'ObjectName':'Student',
            'MethodId':'48c65ad4-1dff-450e-ae6f-744a1105de32',
            'RuleId':'RuleGuid3',
            'ParamValues':
                {
                    'Name':'test',
                    'Age':21,
                    'Birthday':'1998-10-10'
                }
        }
    ]
}";
            return JObject.Parse(strCallMethodDatas.Replace('\'', '"'));
        }

        [HttpGet]
        [Route("SqlExpressTest")]
        public async Task<object> SqlExpressTest()
        {
            //ExpressSqlModel expressSqlModel = new ExpressSqlModel()
            //{
            //    SqlExpressType = ESqlExpressType.And,
            //    Children = new List<ExpressSqlModel>()
            //    {
            //        new ExpressSqlModel()
            //        {
            //            SqlExpressType = ESqlExpressType.And,
            //            Children = new List<ExpressSqlModel>()
            //            {
            //                new ExpressSqlModel()
            //                {
            //                    SqlExpressType = ESqlExpressType.Condition,
            //                    ConditionType = EConditionType.等于,
            //                    Field = "Name1",
            //                    Value = 11
            //                }
            //            }
            //        },
            //        new ExpressSqlModel()
            //        {
            //            SqlExpressType = ESqlExpressType.Condition,
            //            ConditionType = EConditionType.Between,
            //            Field = "Name2",
            //            Value = new ArrayList(){"11","ss"}
            //        },
            //        new ExpressSqlModel()
            //        {
            //            SqlExpressType = ESqlExpressType.Condition,
            //            ConditionType =  EConditionType.等于,
            //            Field="Name3",
            //            Value = 2
            //        },
            //        new ExpressSqlModel()
            //        {
            //            SqlExpressType = ESqlExpressType.Or,
            //            Children = new List<ExpressSqlModel>()
            //            {
            //                new ExpressSqlModel()
            //                {
            //                    SqlExpressType = ESqlExpressType.Condition,
            //                    ConditionType =  EConditionType.等于,
            //                    Field="Name4",
            //                    Value = 3
            //                },
            //                new ExpressSqlModel()
            //                {
            //                    SqlExpressType = ESqlExpressType.Condition,
            //                    ConditionType =  EConditionType.In,
            //                    Field="Name5",
            //                    Value = new ArrayList(){"11","ss","dd","ss"}
            //                },
            //                new ExpressSqlModel()
            //                {
            //                    SqlExpressType = ESqlExpressType.And,
            //                    Children = new List<ExpressSqlModel>()
            //                    {
            //                        new ExpressSqlModel()
            //                        {
            //                            SqlExpressType = ESqlExpressType.Condition,
            //                            ConditionType =  EConditionType.等于,
            //                            Field="Name6",
            //                            Value = 5
            //                        },
            //                        new ExpressSqlModel()
            //                        {
            //                            SqlExpressType = ESqlExpressType.Condition,
            //                            ConditionType =  EConditionType.NotIn,
            //                            Field="Name7",
            //                            Value = new ArrayList(){1,2,3,44,77}
            //                        }
            //                    }
            //                },
            //            }
            //        }

            //    }
            //};

            ExpressSqlModel expressSqlModel = new ExpressSqlModel()
            {
                SqlExpressType = ESqlExpressType.Or,
                Children = new List<ExpressSqlModel>()
                 {
                     new ExpressSqlModel()
                     {
                        Field = "Age",
                        ConditionType = EConditionType.小于,
                        Value = 10,
                        SqlExpressType = ESqlExpressType.Condition
                     },
                     new ExpressSqlModel()
                     {
                         SqlExpressType = ESqlExpressType.And,
                         Children = new List<ExpressSqlModel>()
                         {
                             new ExpressSqlModel()
                             {
                                Field = "Name",
                                ConditionType = EConditionType.In,
                                Value = new ArrayList(){ "chen2","fang2","kuang"},
                                SqlExpressType = ESqlExpressType.Condition
                             },
                             new ExpressSqlModel()
                             {
                                Field = "Age",
                                ConditionType = EConditionType.大于,
                                Value = 20,
                                SqlExpressType = ESqlExpressType.Condition
                             },
                         }
                     }
                 }
            };

            JObject sqlWhereParamValues = new JObject();

            var strSql = await Task.FromResult(ExpressSqlHelper.CreateSqlWhere(expressSqlModel, sqlWhereParamValues, ExpressSqlHelper.TestCreateConditionSql));
            return await Task.FromResult(new
            {
                StrSql = strSql,
                SqlWhereParamValues = sqlWhereParamValues,
                ExpressSqlModel = expressSqlModel
            });
        }

        [HttpGet]
        [Route("GetFormData")]
        public async Task<string> GetFormData()
        {
            SpriteFormData spriteFormData = new SpriteFormData();
            StringBuilder sbTemplate = new StringBuilder();
            sbTemplate.Append(@"<a-table v-bind='dynamicprops'>
    <a slot='name' slot-scope='text'>{{ text }}</a>
    <span slot='customTitle'><a-icon type='smile-o' /> Name</span>
    <span slot='tags' slot-scope='tags'>
      <a-tag
        v-for='tag in tags'
        :key='tag'
      >
        {{ tag.toUpperCase() }}
      </a-tag>
    </span>
    <span slot='action' slot-scope='text, record'>
      <a>Invite 一 {{ record.name }}</a>
      <a-divider type='vertical' />
      <a>Delete</a>
      <a-divider type='vertical' />
      <a class='ant-dropdown-link'> More actions <a-icon type='down' /> </a>
    </span>
  </a-table>");

            string strColumns = @"
[
  {
    dataIndex: 'name',
    key: 'name',
    slots: { title: 'customTitle' },
    scopedSlots: { customRender: 'name' },
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    scopedSlots: { customRender: 'tags' },
  },
  {
    title: 'Action',
    key: 'action',
    scopedSlots: { customRender: 'action' },
  },
]
";

            string strData = @"
[
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
    tags: ['nice', 'developer'],
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
    tags: ['loser'],
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
]
";

            spriteFormData.props = JArray.Parse("['dynamicprops']".Replace('\'', '"'));

            //spriteFormData.data = new
            //{
            //    columns = JArray.Parse(strColumns.Replace('\'', '"')),
            //    data = JArray.Parse(strData.Replace('\'', '"'))
            //};
            spriteFormData.template = sbTemplate.ToString();

            return JsonConvert.SerializeObject(spriteFormData, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }
    }

    public class SpriteFormData
    {
        public string template { get; set; }
        public object props { get; set; }
        public object data { get; set; }
    }
}
