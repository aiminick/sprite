﻿using CK.Sprite.Framework;
using CK.Sprite.ThirdContract;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Test
{
    public class WebTestModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddHttpContextAccessor();

            Services.AddAssemblyOf<WebTestModule>();

            Services.AddApiThirdRemoteService();

            var configuration = Services.GetConfiguration();
            Configure<CrossCutting.SpriteConfig>(configuration.GetSection("SpriteConfig"));
            Configure<DefaultDbConfig>(configuration.GetSection("DefaultDbConfig"));

            Configure<DefaultTenantStoreOptions>(configuration.GetSection("DefaultTenantStoreOptions"));

            Services.AddTestCurrentInfo();
        }
    }
}
