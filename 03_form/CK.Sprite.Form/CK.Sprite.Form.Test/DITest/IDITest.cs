﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Test
{
    public interface IDITest
    {
        string GetString();
    }
}
