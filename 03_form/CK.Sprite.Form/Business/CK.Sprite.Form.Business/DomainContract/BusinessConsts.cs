﻿using CK.Sprite.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Sprite.Form.Business
{
    public class BusinessConsts
    {
        public static string BusinessConnProviderKey { get { return $"{EConnectionType.MySql}_Business"; } }
    }
}
