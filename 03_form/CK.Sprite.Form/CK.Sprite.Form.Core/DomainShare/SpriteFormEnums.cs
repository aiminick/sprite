﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// 表单类型
    /// </summary>
    public enum EFormType
    {
        NormalForm = 1,
        TabForm = 2,
        StepForm = 3,
        DivForm = 4,
        CollapseForm = 5
    }
}
