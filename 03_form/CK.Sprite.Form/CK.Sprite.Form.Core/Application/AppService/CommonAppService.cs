﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Authorize]
    public class CommonAppService : AppService, ICommonAppService
    {
        private readonly CommonService _commonService;
        public CommonAppService(CommonService commonService)
        {
            _commonService = commonService;
        }

        #region WrapInfo Operate

        public async Task AddWrapInfo(WrapInfoCreateDto wrapInfoCreateDto)
        {
            await _commonService.AddWrapInfo(wrapInfoCreateDto);
        }

        public async Task UpdateWrapInfo(WrapInfoUpdateDto wrapInfoUpdateDto)
        {
            await _commonService.UpdateWrapInfo(wrapInfoUpdateDto);
        }

        public async Task DeleteWrapInfo(Guid id)
        {
            await _commonService.DeleteWrapInfo(id);
        }

        public async Task<List<WrapInfoDto>> GetListWrapInfoAsync(Guid businessId)
        {
            var results = await _commonService.GetListWrapInfoAsync(businessId);
            return Mapper.Map<List<WrapInfoDto>>(results);
        }

        public async Task<WrapInfoDto> GetWrapInfoByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<WrapInfo>("WrapInfos",id);
            return Mapper.Map<WrapInfoDto>(dbData);
        }

        #endregion

        #region Control Operate

        public async Task AddControl(ControlCreateDto controlCreateDto)
        {
            await _commonService.AddControl(controlCreateDto);
        }

        public async Task UpdateControl(ControlUpdateDto controlUpdateDto)
        {
            await _commonService.UpdateControl(controlUpdateDto);
        }

        public async Task DeleteControl(Guid id)
        {
            await _commonService.DeleteControl(id);
        }

        public async Task<List<ControlDto>> GetListControlAsync(Guid businessId)
        {
            var results = await _commonService.GetListControlAsync(businessId);
            return Mapper.Map<List<ControlDto>>(results);
        }

        public async Task<ControlDto> GetControlByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<Control>("Controls", id);
            return Mapper.Map<ControlDto>(dbData);
        }

        #endregion

        #region SpriteRule Operate

        public async Task AddSpriteRule(SpriteRuleCreateDto spriteRuleCreateDto)
        {
            await _commonService.AddSpriteRule(spriteRuleCreateDto);
        }

        public async Task UpdateSpriteRule(SpriteRuleUpdateDto spriteRuleUpdateDto)
        {
            await _commonService.UpdateSpriteRule(spriteRuleUpdateDto);
        }

        public async Task DeleteSpriteRule(Guid id)
        {
            await _commonService.DeleteSpriteRule(id);
        }

        public async Task<List<SpriteRuleDto>> GetListSpriteRuleAsync(Guid businessId)
        {
            var results = await _commonService.GetListSpriteRuleAsync(businessId);
            return Mapper.Map<List<SpriteRuleDto>>(results);
        }

        public async Task<SpriteRuleDto> GetSpriteRuleByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<SpriteRule>("SpriteRules", id);
            return Mapper.Map<SpriteRuleDto>(dbData);
        }

        #endregion

        #region RuleAction Operate

        public async Task AddRuleAction(RuleActionCreateDto ruleActionCreateDto)
        {
            await _commonService.AddRuleAction(ruleActionCreateDto);
        }

        public async Task UpdateRuleAction(RuleActionUpdateDto ruleActionUpdateDto)
        {
            await _commonService.UpdateRuleAction(ruleActionUpdateDto);
        }

        public async Task DeleteRuleAction(Guid id)
        {
            await _commonService.DeleteRuleAction(id);
        }

        public async Task<List<RuleActionDto>> GetListRuleActionAsync(Guid ruleId)
        {
            var results = await _commonService.GetListRuleActionAsync(ruleId);
            return Mapper.Map<List<RuleActionDto>>(results);
        }

        public async Task<RuleActionDto> GetRuleActionByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<RuleAction>("RuleActions", id);
            return Mapper.Map<RuleActionDto>(dbData);
        }

        #endregion
    }
}
