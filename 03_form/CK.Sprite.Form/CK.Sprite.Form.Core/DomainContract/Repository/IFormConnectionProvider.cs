﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IFormConnectionProvider : IConnectionProvider
    {
        /// <summary>
        /// 获取业务数据库处理类
        /// </summary>
        /// <param name="unitOfWork">工作单元</param>
        /// <returns></returns>
        IAutoBusinessDbService GetAutoBusinessDbService(IUnitOfWork unitOfWork);
    }
}
