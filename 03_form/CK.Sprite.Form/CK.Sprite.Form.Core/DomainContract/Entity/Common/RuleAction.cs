﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("RuleActions")]
    public class RuleAction : GuidEntityBase
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// 规则Id
        /// </summary>
        public Guid RuleId { get; set; }

        /// <summary>
        /// 子表单Id
        /// </summary>
        public Guid? SubFormId { get; set; }

        /// <summary>
        /// 子视图Id
        /// </summary>
        public Guid? SubViewId { get; set; }

        /// <summary>
        /// 控件Id(Modal等Wrap对象为控件Id+特定属性标识)
        /// </summary>
        public string ObjId { get; set; }

        /// <summary>
        /// 设置属性=1,执行方法=2,绑定数据=3,条件判断=4,停止执行=5,弹出确认=6,执行其他规则=7
        /// </summary>
        public EActionType ActionType { get; set; }

        /// <summary>
        /// 执行配置
        /// </summary>
        public string ActionConfig { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }
    }
}
