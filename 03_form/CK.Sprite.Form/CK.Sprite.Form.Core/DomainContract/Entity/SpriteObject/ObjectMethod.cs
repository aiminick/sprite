﻿using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("ObjectMethods")]
    public class ObjectMethod : DesignEntityBase
    {
        /// <summary>
        /// 方法名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Object外键
        /// </summary>
        public Guid ObjectId { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Execute方法基础
        /// </summary>
        public string SqlBaseMethod { get; set; }

        /// <summary>
        /// 方法类型
        /// </summary>
        public EMethodType MethodType { get; set; }

        /// <summary>
        /// 方法执行类型
        /// </summary>
        public EMethodExeType MethodExeType { get; set; }

        /// <summary>
        /// 方法执行内容，Sql语句时，执行的是Sql命令；反射时，传递程序集以方法，通过反射执行；微服务为微服务名称
        /// </summary>
        public string MethodExeContent { get; set; }

        /// <summary>
        /// 目标应用Code(微服务方法时才赋值)
        /// </summary>
        public string DestApplicationCode { get; set; }

        /// <summary>
        /// 参数冗余存储
        /// </summary>
        public string ParamJsons { get; set; }
    }
}
