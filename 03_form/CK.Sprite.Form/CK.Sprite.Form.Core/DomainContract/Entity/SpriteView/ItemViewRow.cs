﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("ItemViewRows")]
    public class ItemViewRow : GuidEntityBase
    {
        /// <summary>
        /// 视图Id
        /// </summary>
        public Guid ViewId { get; set; }

        /// <summary>
        /// 视图设置
        /// </summary>
        public string PropertySettings { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// 子对象临时存储（序列化时使用）
        /// </summary>
        [Dapper.Contrib.Extensions.Computed]
        public object Children { get; set; }
    }
}
