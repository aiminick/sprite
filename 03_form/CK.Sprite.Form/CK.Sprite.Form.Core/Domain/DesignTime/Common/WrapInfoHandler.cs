﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class WrapInfoHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddWrapInfo(WrapInfo wrapInfo, GuidRepositoryBase<WrapInfo> wrapInfoRepositoryParam = null)
        {
            var wrapInfoRepository = wrapInfoRepositoryParam == null ? new GuidRepositoryBase<WrapInfo>(DesignUnitOfWork) : wrapInfoRepositoryParam;
            return await wrapInfoRepository.InsertAsync(wrapInfo);
        }

        public async Task<bool> UpdateWrapInfo(WrapInfo wrapInfo, GuidRepositoryBase<WrapInfo> wrapInfoRepositoryParam = null)
        {
            var wrapInfoRepository = wrapInfoRepositoryParam == null ? new GuidRepositoryBase<WrapInfo>(DesignUnitOfWork) : wrapInfoRepositoryParam;
            return await wrapInfoRepository.UpdateAsync(wrapInfo);
        }

        public async Task DeleteWrapInfo(WrapInfo wrapInfo, GuidRepositoryBase<WrapInfo> wrapInfoRepositoryParam = null)
        {
            var wrapInfoRepository = wrapInfoRepositoryParam == null ? new GuidRepositoryBase<WrapInfo>(DesignUnitOfWork) : wrapInfoRepositoryParam;
            await wrapInfoRepository.DeleteAsync(wrapInfo);
        }
    }
}
