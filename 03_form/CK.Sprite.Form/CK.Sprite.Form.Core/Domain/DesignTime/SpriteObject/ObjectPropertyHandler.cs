﻿using AutoMapper;
using CK.Sprite.Framework;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class ObjectPropertyHandler : AbstractSpriteObjectHandler, ITransientDependency
    {
        protected override async Task DoAddSpriteObjectAsync(SpriteObjectDto spriteObjectDto)
        {
            if(spriteObjectDto.ObjectPropertyDtos != null && spriteObjectDto.ObjectPropertyDtos.Count > 0)
            {
                List<ObjectProperty> objectProperties = Mapper.Map<List<ObjectProperty>>(spriteObjectDto.ObjectPropertyDtos);

                GuidRepositoryBase<ObjectProperty> objectPropertyRepository = new GuidRepositoryBase<ObjectProperty>(DesignUnitOfWork);
                foreach (var objectProperty in objectProperties)
                {
                    CommonConsts.CheckSqlInject(objectProperty.Name);
                }
                await objectPropertyRepository.InsertAsync(objectProperties);
            }
        }

        public async Task<long> AddObjectProperty(ObjectProperty objectProperty, GuidRepositoryBase<ObjectProperty> objectPropertyRepositoryParam = null)
        {
            var objectPropertyRepository = objectPropertyRepositoryParam == null ? new GuidRepositoryBase<ObjectProperty>(DesignUnitOfWork) : objectPropertyRepositoryParam;
            return await objectPropertyRepository.InsertAsync(objectProperty);
        }

        public async Task<bool> UpdateObjectProperty(ObjectProperty objectProperty, GuidRepositoryBase<ObjectProperty> objectPropertyRepositoryParam = null)
        {
            var objectPropertyRepository = objectPropertyRepositoryParam == null ? new GuidRepositoryBase<ObjectProperty>(DesignUnitOfWork) : objectPropertyRepositoryParam;
            return await objectPropertyRepository.UpdateAsync(objectProperty);
        }

        public async Task DeleteObjectProperty(ObjectProperty objectProperty, GuidRepositoryBase<ObjectProperty> objectPropertyRepositoryParam = null)
        {
            var objectPropertyRepository = objectPropertyRepositoryParam == null ? new GuidRepositoryBase<ObjectProperty>(DesignUnitOfWork) : objectPropertyRepositoryParam;
            await objectPropertyRepository.DeleteAsync(objectProperty);
        }
    }
}
