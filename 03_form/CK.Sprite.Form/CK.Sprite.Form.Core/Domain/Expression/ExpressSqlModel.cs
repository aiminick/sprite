﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class ExpressSqlModel
    {
        public ESqlExpressType SqlExpressType { get; set; }
        public string Field { get; set; }
        public EConditionType ConditionType { get; set; }
        public object Value { get; set; }

        public List<ExpressSqlModel> Children { get; set; }
    }

    public class QueryWhereModel
    {
        /// <summary>
        /// 查询字段名称
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// 等于 = 1,不等于 = 2,Between = 3,In = 4,Like = 5,大于 = 6,大于等于 = 7,小于 = 8,小于等于 = 9,Null = 10,NotNull = 11,NotIn = 12
        /// </summary>
        public EConditionType ConditionType { get; set; }

        /// <summary>
        /// **传递集合时，直接传递数组**
        /// </summary>
        public object Value { get; set; }
    }

    /// <summary>
    /// Sql 表达式树
    /// </summary>
    public enum ESqlExpressType
    {
        And = 1,
        Or = 2,
        Condition = 3
    }
}
