﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class WrapInfoDto : WrapInfoUpdateDto
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }
    }

    [Serializable]
    public class WrapInfoCreateDto : WrapInfoUpdateDto
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }
    }

    [Serializable]
    public class WrapInfoUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 视图或者表单layout组件
        /// </summary>
        public string ContentComponent { get; set; }

        /// <summary>
        /// 视图或者表单Id
        /// </summary>
        public Guid? ObjId { get; set; }

        /// <summary>
        /// 存储List<ComponentName,WrapSetting>序列化后对象
        /// </summary>
        public string WrapConfigs { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

    }
}
