﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface ISpriteViewAppService : IAppService
    {
        #region SpriteView Operate

        Task AddSpriteViewAsync(SpriteViewCreateDto spriteViewCreateDto);

        Task UpdateSpriteViewAsync(SpriteViewUpdateDto spriteViewUpdateDto);

        Task DeleteSpriteView(Guid id);

        Task DeleteSelfSpriteView(Guid id);

        Task<List<SpriteViewDto>> GetSpriteViewAsync(string applicationCode = "Default", int? viewType = null, string filter = default, string category = default);

        Task<List<string>> GetCategorysAsync();

        Task<SpriteViewDto> GetSpriteViewByIdAsync(Guid id);

        #endregion

        #region ListViewFilter Operate

        Task AddListViewFilter(ListViewFilterCreateDto listViewFilterCreateDto);

        Task UpdateListViewFilter(ListViewFilterUpdateDto listViewFilterUpdateDto);

        Task DeleteListViewFilter(Guid id);

        Task<List<ListViewFilterDto>> GetListListViewFilterAsync(Guid viewId);

        Task<ListViewFilterDto> GetListViewFilterByIdAsync(Guid id);

        #endregion

        #region ListViewCustomerColumn Operate

        Task AddListViewCustomerColumn(ListViewCustomerColumnCreateDto listViewCustomerColumnCreateDto);

        Task UpdateListViewCustomerColumn(ListViewCustomerColumnUpdateDto listViewCustomerColumnUpdateDto);

        Task DeleteListViewCustomerColumn(Guid id);

        Task<List<ListViewCustomerColumnDto>> GetListListViewCustomerColumnAsync(Guid viewId);

        Task<ListViewCustomerColumnDto> GetListViewCustomerColumnByIdAsync(Guid id);

        #endregion

        #region ItemViewRow Operate

        Task AddItemViewRow(ItemViewRowCreateDto itemViewRowCreateDto);

        Task UpdateItemViewRow(ItemViewRowUpdateDto itemViewRowUpdateDto);

        Task DeleteItemViewRow(Guid id);

        Task<List<ItemViewRowDto>> GetListItemViewRowAsync(Guid viewId);

        Task<ItemViewRowDto> GetItemViewRowByIdAsync(Guid id);

        #endregion

        #region ItemViewCol Operate

        Task AddItemViewCol(ItemViewColCreateDto itemViewColCreateDto);

        Task UpdateItemViewCol(ItemViewColUpdateDto itemViewColUpdateDto);

        Task DeleteItemViewCol(Guid id);

        Task<List<ItemViewColDto>> GetListItemViewColAsync(Guid itemViewRowId);

        Task<ItemViewColDto> GetItemViewColByIdAsync(Guid id);

        #endregion
    }
}
