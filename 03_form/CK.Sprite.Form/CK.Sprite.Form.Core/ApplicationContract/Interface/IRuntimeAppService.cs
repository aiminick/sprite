﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IRuntimeAppService : IAppService
    {
        Task<List<RemoteSourceDto>> GetDictItemRemoteInfo(string dictCode);

        #region 远程控件调用

        Task<List<ValueName>> DoGetRemoteSelectCall(DoGetRemoteSelectCallInput selectCallInput);

        Task<List<ValueName>> DoGetByIds(DoGetByIdsInput byIdsInput);

        Task<List<ValueName>> DoGetUserByIds(DoGetUserByIdsInput userByIdsInput);

        #endregion

        #region 导入Excel相关

        Task ImportExcel(string applicationCode, JArray importJArray, JArray updateJArray, SpriteObjectDto spriteObjectDto);

        Task<JObject> DoGetUniqInfos(string applicationCode, SpriteObjectDto spriteObjectDto, string uniqFieldInfo, List<string> uniqValues);

        #endregion

        #region 客户端获取视图表单定义信息

        /// <summary>
        /// 本地未找到表单信息，获取表单及视图关联定义详细信息
        /// </summary>
        /// <param name="applicationCode">应用程序Code</param>
        /// <param name="id">表单Id</param>
        /// <param name="dictVersion">客户端字典版本</param>
        /// <returns></returns>
        Task<FormViewVueInfos> GetFormVueInfos(string applicationCode, Guid id, string dictVersion);

        /// <summary>
        /// 本地未找到视图信息，获取视图及表单关联定义详细信息
        /// </summary>
        /// <param name="applicationCode">应用程序Code</param>
        /// <param name="id">视图Id</param>
        /// <param name="dictVersion">客户端字典版本</param>
        /// <returns></returns>
        Task<FormViewVueInfos> GetViewVueInfos(string applicationCode, Guid id, string dictVersion);

        /// <summary>
        /// 本地找到表单信息，获取表单及视图关联变更定义详细信息
        /// </summary>
        /// <param name="relationInfoInputs">客户端存储信息</param>
        /// <returns></returns>
        Task<FormViewVueInfos> GetFormViewVueInfoByRelation(RelationInfoInputs relationInfoInputs);

        /// <summary>
        /// 获取字典信息
        /// </summary>
        /// <param name="version">字典版本号</param>
        /// <returns></returns>
        Task<DictWrap> GetDictInfos(string version);

        #endregion
    }
}
