﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface ISpriteObjectAppService : IAppService
    {
        /// <summary>
        /// 新增object
        /// </summary>
        /// <param name="spriteObjectDto">object实体</param>
        /// <returns></returns>
        Task AddSpriteObjectAsync(SpriteObjectCreateDto spriteObjectCreateDto);

        Task UpdateSpriteObject(SpriteObjectUpdateDto spriteObjectUpdateDto);

        Task<List<SpriteObjectDto>> GetListSpriteObjectAsync(string applicationCode = "Default", int? keyType = null, string filter = default);

        Task<SpriteObjectDto> GetSpriteObjectByIdAsync(Guid id);

        #region ObjectProperty Operate

       Task AddObjectProperty(ObjectPropertyCreateDto objectPropertyCreateDto);

       Task UpdateObjectProperty(ObjectPropertyUpdateDto objectPropertyUpdateDto);

       Task DeleteObjectProperty(Guid id);

       Task<List<ObjectPropertyDto>> GetListObjectPropertyAsync(Guid objectId);

        Task<ObjectPropertyDto> GetObjectPropertyByIdAsync(Guid id);

        #endregion

        #region ObjectMethod Operate

        Task AddObjectMethod(ObjectMethodCreateDto objectMethodCreateDto);

        Task UpdateObjectMethod(ObjectMethodUpdateDto objectMethodUpdateDto);

        Task DeleteObjectMethod(Guid id);

        Task<List<ObjectMethodDto>> GetListObjectMethodAsync(Guid objectId);

        Task<ObjectMethodDto> GetObjectMethodByIdAsync(Guid id);

        #endregion
    }
}
