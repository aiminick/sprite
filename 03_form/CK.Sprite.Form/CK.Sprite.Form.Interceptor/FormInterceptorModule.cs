﻿using CK.Sprite.Framework;
using Microsoft.Extensions.DependencyInjection;

namespace CK.Sprite.Form.Interceptor
{
    public class FormInterceptorModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddAssemblyOf<FormInterceptorModule>();
            Services.OnRegistred(FormInterceptorRegistrar.RegisterIfNeeded);
        }
    }
}
