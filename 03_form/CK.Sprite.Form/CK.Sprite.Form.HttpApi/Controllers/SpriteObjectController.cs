﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Core;
using CK.Sprite.Framework;

namespace CK.Sprite.Form.Controllers
{
    [ApiController]
    [Area("spriteform")]
    [ControllerName("SpriteObject")]
    [Route("api/spriteform/spriteobject")]
    public class SpriteObjectController : Controller, ITransientDependency
    {
        private readonly ISpriteObjectAppService _spriteObjectAppService;

        public SpriteObjectController(ISpriteObjectAppService spriteObjectAppService)
        {
            _spriteObjectAppService = spriteObjectAppService;
        }

        /// <summary>
        /// 创建SpriteObject
        /// </summary>
        /// <param name="spriteObjectCreateDto">SpriteObject实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddSpriteObjectAsync")]
        public async Task AddSpriteObjectAsync(SpriteObjectCreateDto spriteObjectCreateDto)
        {
            await _spriteObjectAppService.AddSpriteObjectAsync(spriteObjectCreateDto);
        }

        [HttpPost]
        [Route("UpdateSpriteObject")]
        public async Task UpdateSpriteObject(SpriteObjectUpdateDto spriteObjectUpdateDto)
        {
            await _spriteObjectAppService.UpdateSpriteObject(spriteObjectUpdateDto);
        }

        [HttpGet]
        [Route("GetListSpriteObjectAsync")]
        public async Task<List<SpriteObjectDto>> GetListSpriteObjectAsync(string applicationCode = "Default", int? keyType = null, string filter = default)
        {
            return await _spriteObjectAppService.GetListSpriteObjectAsync(applicationCode, keyType, filter);
        }

        [HttpGet]
        [Route("GetSpriteObjectByIdAsync")]
        public async Task<SpriteObjectDto> GetSpriteObjectByIdAsync(Guid id)
        {
            return await _spriteObjectAppService.GetSpriteObjectByIdAsync(id);
        }

        [HttpPost]
        [Route("AddObjectProperty")]
        public async Task AddObjectProperty(ObjectPropertyCreateDto objectPropertyCreateDto)
        {
            await _spriteObjectAppService.AddObjectProperty(objectPropertyCreateDto);
        }

        [HttpPost]
        [Route("UpdateObjectProperty")]
        public async Task UpdateObjectProperty(ObjectPropertyUpdateDto objectPropertyUpdateDto)
        {
            await _spriteObjectAppService.UpdateObjectProperty(objectPropertyUpdateDto);
        }

        [HttpGet]
        [Route("DeleteObjectProperty")]
        public async Task DeleteObjectProperty(Guid id)
        {
            await _spriteObjectAppService.DeleteObjectProperty(id);
        }

        [HttpGet]
        [Route("GetListObjectPropertyAsync")]
        public async Task<List<ObjectPropertyDto>> GetListObjectPropertyAsync(Guid objectId)
        {
            return await _spriteObjectAppService.GetListObjectPropertyAsync(objectId);
        }

        [HttpGet]
        [Route("GetObjectPropertyByIdAsync")]
        public async Task<ObjectPropertyDto> GetObjectPropertyByIdAsync(Guid id)
        {
            return await _spriteObjectAppService.GetObjectPropertyByIdAsync(id);
        }

        [HttpPost]
        [Route("AddObjectMethod")]
        public async Task AddObjectMethod(ObjectMethodCreateDto objectMethodCreateDto)
        {
            await _spriteObjectAppService.AddObjectMethod(objectMethodCreateDto);
        }

        [HttpPost]
        [Route("UpdateObjectMethod")]
        public async Task UpdateObjectMethod(ObjectMethodUpdateDto objectMethodUpdateDto)
        {
            await _spriteObjectAppService.UpdateObjectMethod(objectMethodUpdateDto);
        }

        [HttpGet]
        [Route("DeleteObjectMethod")]
        public async Task DeleteObjectMethod(Guid id)
        {
            await _spriteObjectAppService.DeleteObjectMethod(id);
        }

        [HttpGet]
        [Route("GetListObjectMethodAsync")]
        public async Task<List<ObjectMethodDto>> GetListObjectMethodAsync(Guid objectId)
        {
            return await _spriteObjectAppService.GetListObjectMethodAsync(objectId);
        }

        [HttpGet]
        [Route("GetObjectMethodByIdAsync")]
        public async Task<ObjectMethodDto> GetObjectMethodByIdAsync(Guid id)
        {
            return await _spriteObjectAppService.GetObjectMethodByIdAsync(id);
        }
    }
}
