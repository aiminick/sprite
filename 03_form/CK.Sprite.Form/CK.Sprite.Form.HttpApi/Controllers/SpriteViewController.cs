﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Core;
using CK.Sprite.Framework;

namespace CK.Sprite.Form.Controllers
{
    [ApiController]
    [Area("spriteform")]
    [ControllerName("SpriteView")]
    [Route("api/spriteform/spriteview")]
    public class SpriteViewController : Controller, ITransientDependency
    {
        private readonly ISpriteViewAppService _spriteViewAppService;

        public SpriteViewController(ISpriteViewAppService spriteViewAppService)
        {
            _spriteViewAppService = spriteViewAppService;
        }

        [HttpPost]
        [Route("AddItemViewCol")]
        public async Task AddItemViewCol(ItemViewColCreateDto itemViewColCreateDto)
        {
            await _spriteViewAppService.AddItemViewCol(itemViewColCreateDto);
        }

        [HttpPost]
        [Route("AddItemViewRow")]
        public async Task AddItemViewRow(ItemViewRowCreateDto itemViewRowCreateDto)
        {
            await _spriteViewAppService.AddItemViewRow(itemViewRowCreateDto);
        }

        [HttpPost]
        [Route("AddListViewCustomerColumn")]
        public async Task AddListViewCustomerColumn(ListViewCustomerColumnCreateDto listViewCustomerColumnCreateDto)
        {
            await _spriteViewAppService.AddListViewCustomerColumn(listViewCustomerColumnCreateDto);
        }

        [HttpPost]
        [Route("AddListViewFilter")]
        public async Task AddListViewFilter(ListViewFilterCreateDto listViewFilterCreateDto)
        {
            await _spriteViewAppService.AddListViewFilter(listViewFilterCreateDto);
        }

        [HttpPost]
        [Route("AddSpriteViewAsync")]
        public async Task AddSpriteViewAsync(SpriteViewCreateDto spriteViewCreateDto)
        {
            await _spriteViewAppService.AddSpriteViewAsync(spriteViewCreateDto);
        }

        [HttpGet]
        [Route("DeleteItemViewCol")]
        public async Task DeleteItemViewCol(Guid id)
        {
            await _spriteViewAppService.DeleteItemViewCol(id);
        }

        [HttpGet]
        [Route("DeleteItemViewRow")]
        public async Task DeleteItemViewRow(Guid id)
        {
            await _spriteViewAppService.DeleteItemViewRow(id);
        }

        [HttpGet]
        [Route("DeleteListViewCustomerColumn")]
        public async Task DeleteListViewCustomerColumn(Guid id)
        {
            await _spriteViewAppService.DeleteListViewCustomerColumn(id);
        }

        [HttpGet]
        [Route("DeleteListViewFilter")]
        public async Task DeleteListViewFilter(Guid id)
        {
            await _spriteViewAppService.DeleteListViewFilter(id);
        }

        [HttpGet]
        [Route("DeleteSpriteView")]
        public async Task DeleteSpriteView(Guid id)
        {
            await _spriteViewAppService.DeleteSpriteView(id);
        }

        [HttpGet]
        [Route("DeleteSelfSpriteView")]
        public async Task DeleteSelfSpriteView(Guid id)
        {
            await _spriteViewAppService.DeleteSelfSpriteView(id);
        }

        [HttpGet]
        [Route("GetListItemViewColAsync")]
        public async Task<List<ItemViewColDto>> GetListItemViewColAsync(Guid itemViewRowId)
        {
            return await _spriteViewAppService.GetListItemViewColAsync(itemViewRowId);
        }

        [HttpGet]
        [Route("GetItemViewColByIdAsync")]
        public async Task<ItemViewColDto> GetItemViewColByIdAsync(Guid id)
        {
            return await _spriteViewAppService.GetItemViewColByIdAsync(id);
        }

        [HttpGet]
        [Route("GetListItemViewRowAsync")]
        public async Task<List<ItemViewRowDto>> GetListItemViewRowAsync(Guid viewId)
        {
            return await _spriteViewAppService.GetListItemViewRowAsync(viewId);
        }

        [HttpGet]
        [Route("GetItemViewRowByIdAsync")]
        public async Task<ItemViewRowDto> GetItemViewRowByIdAsync(Guid id)
        {
            return await _spriteViewAppService.GetItemViewRowByIdAsync(id);
        }

        [HttpGet]
        [Route("GetListListViewCustomerColumnAsync")]
        public async Task<List<ListViewCustomerColumnDto>> GetListListViewCustomerColumnAsync(Guid viewId)
        {
            return await _spriteViewAppService.GetListListViewCustomerColumnAsync(viewId);
        }

        [HttpGet]
        [Route("GetListViewCustomerColumnByIdAsync")]
        public async Task<ListViewCustomerColumnDto> GetListViewCustomerColumnByIdAsync(Guid id)
        {
            return await _spriteViewAppService.GetListViewCustomerColumnByIdAsync(id);
        }

        [HttpGet]
        [Route("GetListListViewFilterAsync")]
        public async Task<List<ListViewFilterDto>> GetListListViewFilterAsync(Guid viewId)
        {
            return await _spriteViewAppService.GetListListViewFilterAsync(viewId);
        }

        [HttpGet]
        [Route("GetListViewFilterByIdAsync")]
        public async Task<ListViewFilterDto> GetListViewFilterByIdAsync(Guid id)
        {
            return await _spriteViewAppService.GetListViewFilterByIdAsync(id);
        }

        [HttpGet]
        [Route("GetSpriteViewAsync")]
        public async Task<List<SpriteViewDto>> GetSpriteViewAsync(string applicationCode = "Default", int? viewType = null, string filter = default, string category = default)
        {
            return await _spriteViewAppService.GetSpriteViewAsync(applicationCode, viewType, filter, category);
        }

        [HttpGet]
        [Route("GetCategorysAsync")]
        public async Task<List<string>> GetCategorysAsync()
        {
            return await _spriteViewAppService.GetCategorysAsync();
        }

        [HttpGet]
        [Route("GetSpriteViewByIdAsync")]
        public async Task<SpriteViewDto> GetSpriteViewByIdAsync(Guid id)
        {
            return await _spriteViewAppService.GetSpriteViewByIdAsync(id);
        }

        [HttpPost]
        [Route("UpdateItemViewCol")]
        public async Task UpdateItemViewCol(ItemViewColUpdateDto itemViewColUpdateDto)
        {
            await _spriteViewAppService.UpdateItemViewCol(itemViewColUpdateDto);
        }

        [HttpPost]
        [Route("UpdateItemViewRow")]
        public async Task UpdateItemViewRow(ItemViewRowUpdateDto itemViewRowUpdateDto)
        {
            await _spriteViewAppService.UpdateItemViewRow(itemViewRowUpdateDto);
        }

        [HttpPost]
        [Route("UpdateListViewCustomerColumn")]
        public async Task UpdateListViewCustomerColumn(ListViewCustomerColumnUpdateDto listViewCustomerColumnUpdateDto)
        {
            await _spriteViewAppService.UpdateListViewCustomerColumn(listViewCustomerColumnUpdateDto);
        }

        [HttpPost]
        [Route("UpdateListViewFilter")]
        public async Task UpdateListViewFilter(ListViewFilterUpdateDto listViewFilterUpdateDto)
        {
            await _spriteViewAppService.UpdateListViewFilter(listViewFilterUpdateDto);
        }

        [HttpPost]
        [Route("UpdateSpriteViewAsync")]
        public async Task UpdateSpriteViewAsync(SpriteViewUpdateDto spriteViewUpdateDto)
        {
            await _spriteViewAppService.UpdateSpriteViewAsync(spriteViewUpdateDto);
        }
    }
}
