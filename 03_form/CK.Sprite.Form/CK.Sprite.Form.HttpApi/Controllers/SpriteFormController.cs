﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Core;
using CK.Sprite.Framework;

namespace CK.Sprite.Form.Controllers
{
    [ApiController]
    [Area("spriteform")]
    [ControllerName("SpriteForm")]
    [Route("api/spriteform/spriteform")]
    public class SpriteFormController : Controller, ITransientDependency
    {
        private readonly ISpriteFormAppService _spriteFormAppService;

        public SpriteFormController(ISpriteFormAppService spriteFormAppService)
        {
            _spriteFormAppService = spriteFormAppService;
        }

        [HttpPost]
        [Route("AddFormCol")]
        public async Task AddFormCol(FormColCreateDto formColCreateDto)
        {
            await _spriteFormAppService.AddFormCol(formColCreateDto);
        }

        [HttpPost]
        [Route("AddFormItem")]
        public async Task AddFormItem(FormItemCreateDto formItemCreateDto)
        {
            await _spriteFormAppService.AddFormItem(formItemCreateDto);
        }

        [HttpPost]
        [Route("AddFormRow")]
        public async Task AddFormRow(FormRowCreateDto formRowCreateDto)
        {
            await _spriteFormAppService.AddFormRow(formRowCreateDto);
        }

        [HttpPost]
        [Route("AddSpriteFormAsync")]
        public async Task AddSpriteFormAsync(SpriteFormCreateDto spriteFormCreateDto)
        {
            await _spriteFormAppService.AddSpriteFormAsync(spriteFormCreateDto);
        }

        [HttpGet]
        [Route("DeleteFormCol")]
        public async Task DeleteFormCol(Guid id)
        {
            await _spriteFormAppService.DeleteFormCol(id);
        }

        [HttpGet]
        [Route("DeleteFormItem")]
        public async Task DeleteFormItem(Guid id)
        {
            await _spriteFormAppService.DeleteFormItem(id);
        }

        [HttpGet]
        [Route("DeleteFormRow")]
        public async Task DeleteFormRow(Guid id)
        {
            await _spriteFormAppService.DeleteFormRow(id);
        }

        [HttpGet]
        [Route("DeleteSpriteForm")]
        public async Task DeleteSpriteForm(Guid id)
        {
            await _spriteFormAppService.DeleteSpriteForm(id);
        }

        [HttpGet]
        [Route("DeleteSelfSpriteForm")]
        public async Task DeleteSelfSpriteForm(Guid id)
        {
            await _spriteFormAppService.DeleteSelfSpriteForm(id);
        }

        [HttpGet]
        [Route("GetListFormColAsync")]
        public async Task<List<FormColDto>> GetListFormColAsync(Guid formRowId)
        {
            return await _spriteFormAppService.GetListFormColAsync(formRowId);
        }

        [HttpGet]
        [Route("GetFormColByIdAsync")]
        public async Task<FormColDto> GetFormColByIdAsync(Guid id)
        {
            return await _spriteFormAppService.GetFormColByIdAsync(id);
        }

        [HttpGet]
        [Route("GetListFormItemAsync")]
        public async Task<List<FormItemDto>> GetListFormItemAsync(Guid formId)
        {
            return await _spriteFormAppService.GetListFormItemAsync(formId);
        }

        [HttpGet]
        [Route("GetFormItemByIdAsync")]
        public async Task<FormItemDto> GetFormItemByIdAsync(Guid id)
        {
            return await _spriteFormAppService.GetFormItemByIdAsync(id);
        }

        [HttpGet]
        [Route("GetListFormRowAsync")]
        public async Task<List<FormRowDto>> GetListFormRowAsync(Guid formItemId)
        {
            return await _spriteFormAppService.GetListFormRowAsync(formItemId);
        }

        [HttpGet]
        [Route("GetFormRowByIdAsync")]
        public async Task<FormRowDto> GetFormRowByIdAsync(Guid id)
        {
            return await _spriteFormAppService.GetFormRowByIdAsync(id);
        }

        [HttpGet]
        [Route("GetSpriteFormAsync")]
        public async Task<List<SpriteFormDto>> GetSpriteFormAsync(bool isTemplate, string applicationCode = "Default", int? formType = null, string filter = default, string category = default)
        {
            return await _spriteFormAppService.GetSpriteFormAsync(isTemplate, applicationCode, formType, filter, category);
        }

        [HttpGet]
        [Route("SetFormTemplate")]
        public async Task SetFormTemplate(Guid id, bool isTemplate)
        {
            await _spriteFormAppService.SetFormTemplate(id, isTemplate);
        }

        [HttpGet]
        [Route("GetCategorysAsync")]
        public async Task<List<string>> GetCategorysAsync()
        {
            return await _spriteFormAppService.GetCategorysAsync();
        }

        [HttpGet]
        [Route("GetSpriteFormByIdAsync")]
        public async Task<SpriteFormDto> GetSpriteFormByIdAsync(Guid id)
        {
            return await _spriteFormAppService.GetSpriteFormByIdAsync(id);
        }

        [HttpPost]
        [Route("UpdateFormCol")]
        public async Task UpdateFormCol(FormColUpdateDto formColUpdateDto)
        {
            await _spriteFormAppService.UpdateFormCol(formColUpdateDto);
        }

        [HttpPost]
        [Route("UpdateFormItem")]
        public async Task UpdateFormItem(FormItemUpdateDto formItemUpdateDto)
        {
            await _spriteFormAppService.UpdateFormItem(formItemUpdateDto);
        }

        [HttpPost]
        [Route("UpdateFormRow")]
        public async Task UpdateFormRow(FormRowUpdateDto formRowUpdateDto)
        {
            await _spriteFormAppService.UpdateFormRow(formRowUpdateDto);
        }

        [HttpPost]
        [Route("UpdateSpriteFormAsync")]
        public async Task UpdateSpriteFormAsync(SpriteFormUpdateDto spriteFormUpdateDto)
        {
            await _spriteFormAppService.UpdateSpriteFormAsync(spriteFormUpdateDto);
        }

        #region Form Template

        [HttpPost]
        [Route("CreateFormFromTemplate")]
        public async Task CreateFormFromTemplate(CreateFormFromTemplateInput formFromTemplateInput)
        {
            await _spriteFormAppService.CreateFormFromTemplate(formFromTemplateInput);
        }

        #endregion
    }
}
